package downloader

import (
	"compress/flate"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime"
	"net/http"
	"net/http/cookiejar"
	"os"
	"path/filepath"
	"strings"

	"github.com/andybalholm/brotli"
)

const generalErrorText = "cannot download: %w"

var defaultClient *Client

func init() {
	defaultClient = New()
}

// Client is a new instance of the Downloader Client as an alternative to the default Client.
type Client struct {
	client  http.Client
	headers map[string]string
}

// New returns a new Downloader Client with the default Headers. You can call SetHeader on it to change the headers.
func New() *Client {
	jar, _ := cookiejar.New(nil)
	client := Client{
		client: http.Client{
			Jar: jar,
		},
	}
	headers := make(map[string]string)
	headers["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0)"
	headers["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8"
	headers["Accept-Encoding"] = "gzip, deflate, br"
	headers["DNT"] = "1"
	headers["Connection"] = "keep-alive"
	client.headers = headers
	return &client
}

// SetHeader sets the value for the http headers to be used.
func (c *Client) SetHeader(name string, value string) {
	c.headers[name] = value
}

// Data downloads uri returning the bytes data and its content type.
func (c *Client) Data(uri string) ([]byte, string, error) {
	if uri == "" {
		return nil, "", errors.New("cannot download empty uri")
	}
	if !strings.HasPrefix(uri, "http://") && !strings.HasPrefix(uri, "https://") {
		return nil, "", errors.New("cannot download uri unless it starts with 'http://' or 'https://'")
	}
	request, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		return nil, "", fmt.Errorf("could not prepare http request: %w", err)
	}
	for k, v := range c.headers {
		request.Header.Set(k, v)
	}
	response, err := c.client.Do(request)
	if err != nil {
		return nil, "", fmt.Errorf("could not process http request: %w", err)
	}
	var reader io.Reader
	encoding := response.Header.Get("Content-Encoding")
	switch encoding {
	case "gzip":
		reader, err = gzip.NewReader(response.Body)
		if err != nil {
			return nil, "", fmt.Errorf("response Content-Encoding is gzip, but gzip produced error: %w", err)
		}
	case "deflate":
		reader = flate.NewReader(response.Body)
	case "br":
		reader = brotli.NewReader(response.Body)
	default:
		reader = response.Body
	}
	defer response.Body.Close()
	data, err := io.ReadAll(reader)
	if err != nil {
		return nil, "", fmt.Errorf("could not read http response: %w", err)
	}
	contentType := response.Header.Get("Content-Type")
	return data, contentType, nil
}

// File downloads the file at location uri to path, overwriting an existing file. if path does not exist, it is created. If path ends in a slash it is assumed to have no filename and a filename is inferred from uri, otherwise the filename in path is used. If path provides no extension, an extension is inferred from the uri or from the content-type. If uri starts with 'https', SSL is used.
func (c *Client) File(uri string, path string) error {
	return c.getFile(uri, path, false)
}

// FileLazy works like File() but returns nil early if the file already exists and therefore does not override the file. Note that it only looks for existing files and does not compare file contents.
func (c *Client) FileLazy(uri string, directory string) error {
	return c.getFile(uri, directory, true)
}

func (c *Client) getFile(uri string, directory string, lazy bool) error {
	data, contentType, err := c.Data(uri)
	if err != nil {
		return fmt.Errorf(generalErrorText, err)
	}
	ext, err := getExtension(contentType)
	if err != nil {
		return fmt.Errorf(generalErrorText, err)
	}
	if strings.HasSuffix(directory, "/") {
		directory = correctFilename(directory, uri)
	}
	path := correctExtension(directory, ext)
	dir := filepath.Dir(path)
	if err = os.MkdirAll(dir, os.ModePerm); err != nil {
		return fmt.Errorf("cannot create '%v': %w", dir, err)
	}
	if lazy {
		if _, err := os.Stat(path); !os.IsNotExist(err) {
			return nil // file exists
		}
	}
	if err != nil {
		return fmt.Errorf(generalErrorText + err.Error())
	}
	if err = ioutil.WriteFile(path, data, os.ModePerm); err != nil {
		return fmt.Errorf("cannot write file to '%v': %w", path, err)
	}
	return nil
}

func getExtension(contentType string) (ext string, err error) {
	extensions, err := mime.ExtensionsByType(contentType)
	if err != nil || extensions == nil || len(extensions) == 0 {
		return
	}
	if len(extensions) == 1 {
		ext = extensions[0]
		return
	}
	contentExt := "." + strings.Split(contentType, "/")[1]
	if contentExt == ".jpeg" {
		contentExt = ".jpg" // special case for content type image/jpeg where the common extension would be '.jpg' instead of '.jpeg'.
	}
	for _, e := range extensions {
		if e == contentExt {
			ext = e
			break
		}
	}
	return
}

func correctFilename(path string, uri string) string {
	name := filepath.Base(uri)
	return filepath.Join(path, name)
}

func correctExtension(path string, extension string) string {
	dirExt := filepath.Ext(path)
	if dirExt != extension {
		if len(dirExt) > 4 || (len(dirExt) > 1 && dirExt[1] == ' ') {
			path += extension
		} else {
			path = path[:len(path)-len(dirExt)] + extension
		}
	}
	return path
}

// Data calls Data() on the default Client using the default Headers.
func Data(uri string) ([]byte, string, error) {
	return defaultClient.Data(uri)
}

// File calls File() on the default Client using the default Headers.
func File(uri string, path string) error {
	return defaultClient.File(uri, path)
}

// FileLazy calls FileLazy() on the default Client using the default Headers.
func FileLazy(uri string, path string) error {
	return defaultClient.File(uri, path)
}

// Deprecated: use File() instead.
func GetHTTP(uri string, path string) error {
	return defaultClient.File(uri, path)
}

// Deprecated: use FileLazy() instead.
func GetLazyHTTP(uri string, directory string) error {
	return defaultClient.FileLazy(uri, directory)
}
