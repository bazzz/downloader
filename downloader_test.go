package downloader

import (
	"path/filepath"
	"testing"
)

func TestGetFilenameFromURI(t *testing.T) {
	expected := filepath.Clean("/dir/Bas.jpg")
	result := correctFilename("/dir/", "http://domain.local/Bas.jpg")
	if result != expected {
		t.Fatal("Expected", expected, "but was", result)
	}
}

func TestCannotGetFilenameFromURI(t *testing.T) {
	expected := filepath.Clean("/dir/x")
	result := correctFilename("/dir/", "http://domain.local/x/")
	if result != expected {
		t.Fatal("Expected", expected, "but was", result)
	}
}

func TestMissingExtension(t *testing.T) {
	expected := "/Bas.jpg"
	ext, err := getExtension("image/jpeg")
	if err != nil {
		t.Fatal(err)
	}
	result := correctExtension("/Bas", ext)
	if result != expected {
		t.Fatal("Expected", expected, "but was", result)
	}
}

func TestWrongExtension(t *testing.T) {
	expected := "/Bas.jpg"
	ext, err := getExtension("image/jpeg")
	if err != nil {
		t.Fatal(err)
	}
	result := correctExtension("/Bas.png", ext)
	if result != expected {
		t.Fatal("Expected", expected, "but was", result)
	}
}

func TestMissingExtensionNameHasDot(t *testing.T) {
	expected := "/Bas. Bas.jpg"
	ext, err := getExtension("image/jpeg")
	if err != nil {
		t.Fatal(err)
	}
	result := correctExtension("/Bas. Bas", ext)
	if result != expected {
		t.Fatal("Expected", expected, "but was", result)
	}
}

func TestMissingExtensionNameHasDotAndNoSpace(t *testing.T) {
	expected := "/Bas.BasBas.jpg"
	ext, err := getExtension("image/jpeg")
	if err != nil {
		t.Fatal(err)
	}
	result := correctExtension("/Bas.BasBas", ext)
	if result != expected {
		t.Fatal("Expected", expected, "but was", result)
	}
}
